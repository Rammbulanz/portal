import { TransformNode, FreeCamera, Scene, Vector3, Matrix } from "babylonjs";
import { KeyCodes } from "../enums/KeyCodes";

export interface PlayerOptions {
    camera?: FreeCamera
    bodyHeight?: number
}

export class Player extends TransformNode implements PlayerOptions {

    public camera: FreeCamera;
    public bodyHeight: number = 1.6;

    constructor(name: string, scene: Scene, options?: PlayerOptions) {
        super(name, scene);

        // Merge options into this
        if (options) {
            for (const key in options) {
                this[key] = options[key];
            }
        }

        this.createCamera();
    }

    private createCamera(): void {
        this.camera = new FreeCamera("player", Vector3.Zero(), this.getScene());

        this.camera.checkCollisions = true;
        this.camera.applyGravity = true;

        this.camera.position.y = this.bodyHeight;
        this.camera.speed = 0.5;

        this.camera.keysLeft = [KeyCodes.KEY_A];
        this.camera.keysRight = [KeyCodes.KEY_D];
        this.camera.keysUp = [KeyCodes.KEY_W];
        this.camera.keysDown = [KeyCodes.KEY_S];
        this.camera.attachControl(this.getEngine().getRenderingCanvas());
    }

}