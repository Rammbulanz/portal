import { SceneLoader, Vector3, MeshBuilder, StandardMaterial, CubeTexture, Texture, Color3 } from "babylonjs";
import { App } from "./App";
import { Portal } from "./Portal";
import { Player } from "./Player";

/**
 * This class is the entry point for the game logic.
 */
export class Game {

	public app: App;
	public promise: Promise<Game>;
	public player: Player;

	public portals: Portal[] = [];

	constructor(app: App) {
		this.app = app;
		this.promise = this.initialize();
	}

	protected async initialize(): Promise<Game> {
		await SceneLoader.AppendAsync("assets/", "scene.babylon", this.app.scene);

		this.player = new Player("player", this.app.scene);
		
		this.app.scene.activeCamera = this.player.camera;

		this.createGround();
		this.createSkybox();
		this.createPortals();

		return this;
	}

	public createGround(size: number = 1000): void {
		const ground = MeshBuilder.CreateGround("ground", {
			width: size,
			height: size
		}, this.app.scene);
		ground.checkCollisions = true;
		ground.isPickable = false;

		ground.computeWorldMatrix(true);
		ground.freezeWorldMatrix();

		const material = new StandardMaterial('ground', this.app.scene);
		// material.backFaceCulling = false;
		
		const texture = new Texture("assets/textures/grass.jpg", this.app.scene);
		texture.uScale = texture.vScale = 300;

		material.ambientTexture = texture;		
		material.specularColor = new Color3(0.2, 0.2, 0.2);

		ground.material = material;

	}

	public createSkybox(size: number = 1000): void {
		const skybox = MeshBuilder.CreateBox("skyBox", { size }, this.app.scene);

		skybox.computeWorldMatrix(true);
		skybox.freezeWorldMatrix();

		const skyboxMaterial = new StandardMaterial("skyBox", this.app.scene);
		skyboxMaterial.backFaceCulling = false;
		skyboxMaterial.reflectionTexture = new CubeTexture("assets/textures/skybox/TropicalSunnyDay", this.app.scene);
		skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
		skyboxMaterial.disableLighting = true;
		skybox.material = skyboxMaterial;
	}

	public createPortals() {

		const blue = new Portal("bluePortal", this.app.scene, {
			position: new Vector3(20, 1, 10),
			direction: new Vector3(0, 1, 0)
		});
		blue.position.y = blue.height / 2;

		const orange = new Portal("bluePortal", this.app.scene, {
			position: new Vector3(10, 1, 10),
			direction: new Vector3(0, -1, 0)
		});
		orange.position.y = orange.height / 2;

		this.portals.push(blue, orange);

	}

}