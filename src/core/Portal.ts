import { Vector3, Mesh, Node, Scene, VertexData, StandardMaterial, Color3, Vector4 } from "babylonjs";

export class Portal extends Mesh {

	public direction: Vector3;
	public targetPortal: Portal;

	public width: number = 1.5;
	public height: number = 3;

	constructor(name: string, scene: Scene, options?: any) {
		super(name, scene);
		this.initialize();

		if (options) {
			this.locate(options.position, options.direction);
		}
	}

	private initialize(): void {

		const width = 1.5;
		const height = 3.0;

		const vertexData = VertexData.CreatePlane({
			width,
			height,
			frontUVs: new Vector4(0, 0, 1, 1)
		});

		vertexData.normals = new Float32Array(vertexData.positions.length);
		VertexData.ComputeNormals(vertexData.normals, vertexData.indices, vertexData.normals);
		vertexData.applyToMesh(this);

		const material = new StandardMaterial("portal", this.getScene());
		material.ambientColor = new Color3(1, 1, 0);
		material.backFaceCulling = false;
		this.material = material;

	}

	/**
	 * Update position and rotation
	 * @param position 
	 * @param direction 
	 */
	public locate(position: Vector3 = Vector3.Zero(), direction: Vector3 = Vector3.Forward()): void {
		this.unfreezeWorldMatrix();

		this.position.copyFrom(position);
		// ToDo: Set rotation

		this.computeWorldMatrix(true);
		this.freezeWorldMatrix();
	}

}

Node.AddNodeConstructor("Portal", (name: string, scene: Scene, options) => () => new Portal(name, scene, options));