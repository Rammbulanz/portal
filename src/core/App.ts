import { Engine, Scene } from "babylonjs";
import { Game } from "./Game";


export class App {

	public canvas: HTMLCanvasElement;
	public engine: Engine;
	public scene: Scene;
	public game: Game;

	private _lastRenderSize = { width: 0, height: 0 };
	private _renderFunction: () => void;

	constructor() {
		this.initialize();
	}

	private async initialize(): Promise<void> {

		this.canvas = document.querySelector("#render-target");
		this.engine = new Engine(this.canvas);
		this.scene = new Scene(this.engine);
		this.game = new Game(this);

		await this.game.promise;
		this.startRendering();

		this.canvas.addEventListener("click", this.lockPointer.bind(this));
		this.canvas.addEventListener("blur", this.unlockPointer.bind(this));

	}

	public lockPointer() {

		if (this.engine.isPointerLock) {
			return;
		}

		const requestPointerLock = this.canvas.requestPointerLock
			|| this.canvas.mozRequestPointerLock
			|| this.canvas.webkitRequestPointerLock;

		if (requestPointerLock) {

			requestPointerLock.call(this.canvas);

		} else {
			console.error("No pointerlock available");
			alert("No pointerlock available");
		}

	}
	public unlockPointer() {		
		if (document.exitPointerLock) {
			document.exitPointerLock();
		}
	}

	public startRendering(): void {
		if (!this._renderFunction) {
			this._renderFunction = () => this.animate();
			this.engine.runRenderLoop(this._renderFunction);
		}
	}
	public stopRendering(): void {
		if (this._renderFunction) {
			this.engine.stopRenderLoop(this._renderFunction);
			this._renderFunction = undefined;
		}
	}
	private animate(): void {

		if (this.canvas.clientWidth !== this._lastRenderSize.width || this.canvas.clientHeight !== this._lastRenderSize.height) {
			this.engine.resize();
			this._lastRenderSize.width = this.canvas.clientWidth;
			this._lastRenderSize.height = this.canvas.clientHeight;
		}

		this.scene.render();
	}

}