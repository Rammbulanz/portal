import { Nullable, Scene } from "babylonjs";

export const node = (type: String) => (clazz: any) => (name: string, scene: Nullable<Scene>, options?: any) => () => new clazz(name, scene, options);