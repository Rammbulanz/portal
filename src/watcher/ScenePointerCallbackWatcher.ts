import { ScenePointerWatcher } from "./ScenePointerWatcher";
import { PointerInfo } from "babylonjs";

export class ScenePointerCallbacKWatcher extends ScenePointerWatcher {

    private callback: (pointerInfo: PointerInfo) => void;

    public update(pointerInfo: PointerInfo) {
        this.callback(pointerInfo);
    }

}