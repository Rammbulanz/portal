import { IWatcher } from "./IWatcher";
import { PointerInfo, Scene, Observer } from "babylonjs";

export abstract class ScenePointerWatcher implements IWatcher<PointerInfo> {

    private scene: Scene;
    private observer: Observer<PointerInfo>;

    constructor(scene: Scene) {
        this.scene = scene;
    }

    public start(): void {
        if (!this.observer) {
            this.observer = this.scene.onPointerObservable.add(this.update.bind(this));
        }
    }
    public stop(): void {
        if (this.observer) {
            this.scene.onPointerObservable.remove(this.observer);
            this.observer = undefined;
        }
    }

    public abstract update(p: PointerInfo): void;

}