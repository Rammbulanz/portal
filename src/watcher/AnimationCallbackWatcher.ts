import { AnimationFrameWatcher } from "./AnimationFrameWatcher";

export class AnimationCallbackWatcher extends AnimationFrameWatcher {

    public callback: (time: number) => void;

    public update(time: number) {
        this.callback(time);
    }

}