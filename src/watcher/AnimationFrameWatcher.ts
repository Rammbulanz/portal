import { IWatcher } from "./IWatcher";

export abstract class AnimationFrameWatcher implements IWatcher<number> {

    private handle: number;
    private animate: (time: number) => void;

    public start(): void {

        if (typeof this.handle === "number") {
            return;
        }

        const scope = this;

        scope.animate = (time: number) => {
            scope.update(time);
            scope.handle = requestAnimationFrame(scope.animate);
        }

        scope.handle = requestAnimationFrame(scope.animate);

    }
    public stop(): void {
        if (typeof this.handle === "number") {
            cancelAnimationFrame(this.handle);
            this.handle = undefined;
        }
    }

    public abstract update(time: number): void;
}