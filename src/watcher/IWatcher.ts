export interface IWatcher<S> {
    start(): void;
    stop(): void
    update(state: S): void
}