import { IDisposable } from 'babylonjs';

type Constructor<T={}> = {
    new(...args: any[]): T
}

export const ConnectMixIn = <Base extends Constructor<any>, S=any>(store: import('redux').Store<S>) => (superClass: Base) => class extends superClass implements IDisposable {

    private _unsubscribe: import('redux').Unsubscribe;

    constructor(...args: any[]) {
        super(...args);

        
    }


    public dispose(): void {
        if (this._unsubscribe) {
            this._unsubscribe();
            this._unsubscribe = undefined;
        }
    }

    protected stateChanged(state: S): void { }

};