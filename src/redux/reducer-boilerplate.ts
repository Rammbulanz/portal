import { AnyAction, Reducer } from "redux";

type ReducerHandlers<TState> = Record<string, (state: TState, action: AnyAction) => TState>;

/**
 * Create a redux reducer, where you don't need to case. When a action is dispatched, it will pick and call the handler from the passed `handlers` object.
 * @param initial The initial value for the reducer
 */
export function createReducerBoilerplate<TState>(initial: TState, handlers: ReducerHandlers<TState>): Reducer<TState> {
    return function (state: TState = initial, action: AnyAction)  {
        if (handlers.hasOwnProperty(action.type)) {
            state = handlers[action.type](state, action);
        }
        return state;
    };
} 