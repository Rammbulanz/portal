const path = require("path");

/** @type {import("webpack").Configuration} */
module.exports = {

    mode: "development",

    entry: "./src/entry.ts",

    output: {
        path: path.resolve("dist"),
        filename: "bundle.js",
        publicPath: "/dist/"
    },

    resolve: {
        extensions: ["tsx", ".ts", "jsx", ".js"]
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'awesome-typescript-loader',
                exclude: /node_modules/
            }
        ]
    },

    externals: {
        "babylonjs": "BABYLON"
    },

    // Dev Part
    devtool: "source-map",
    watch: true,
    watchOptions: {
        poll: true
    },
    devServer: {
        port: 8080,
        host: "0.0.0.0",
        disableHostCheck: true
    }

};