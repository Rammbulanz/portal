const _ = require("lodash");
const path = require("path");


const debug = process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase().startsWith("dev") : false;

/** @type {import("webpack").Configuration} */
const config = {

    mode: debug ? "development" : "production",

    entry: "./src/entry.ts",

    output: {
        path: path.resolve("dist"),
        filename: "bundle.js",
        publicPath: "/dist/"
    },

    resolve: {
        extensions: ["tsx", ".ts", "jsx", ".js"]
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'awesome-typescript-loader',
                exclude: /node_modules/
            }
        ]
    },

    externals: {
        "babylonjs": "BABYLON"
    }

}

if (debug) {

    /** @type {import("webpack").Configuration} */
    let devConfig = {
        devtool: "source-map",
        watch: true,
        watchOptions: {
            poll: true
        },
        devServer: {
            port: 8080,
            host: "0.0.0.0",
            disableHostCheck: true
        }
    };

    _.merge(config, devConfig);

}

module.exports = config;